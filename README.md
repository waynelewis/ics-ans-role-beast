ics-ans-role-beast
==================

Ansible role to install BEAST (Best Ever Alarm System Toolkit).

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

Role Variables
--------------

```yaml
beast_version: 4.6.1.0
beast_alarm_server_url: https://artifactory.esss.lu.se/artifactory/CS-Studio/production/{{ beast_version }}/alarm-server-{{ beast_version }}-linux.gtk.x86_64.tar.gz
beast_alarm_config_url: https://artifactory.esss.lu.se/artifactory/CS-Studio/production/{{ beast_version }}/alarm-config-{{ beast_version }}-linux.gtk.x86_64.tar.gz
beast_epics_ca_addr_list: 127.0.0.1
beast_epics_ca_auto_addr_list: "true"

beast_rdb_server: localhost
beast_rdb_name: alarm
beast_rdb_url: jdbc:mysql://{{ beast_rdb_server }}/{{ beast_rdb_name }}
beast_rdb_user: alarm
beast_rdb_password: alarmpwd
beast_rdb_schema: ALARM

beast_jms_server: localhost
beast_jms_url: failover:(tcp://{{ beast_jms_server }}:61616)
beast_jms_user: admin
beast_jms_password: admin

beast_config_repo: https://gitlab.esss.lu.se/ics-infrastructure/beast-config.git
beast_config_repo_version: master
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-beast
```

License
-------

BSD 2-clause
